---
# Display name
name: Erlich Bachman
avatar_image: "admin.jpg"
# Username (this should match the folder name)
authors:
- admin
# resume download button
btn:
- url : "files/cv.pdf"
  label : "Download Resume"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Chief Evangelist Officer at Pied Piper

# Organizations/Affiliations
organizations:
- name:  Pied Piper
  url: "http://www.piedpiper.com/"

# Short bio (displayed in user profile at end of posts)
bio: My interests are business and startups

# Should the user's education and interests be displayed?
display_education: true

interests:
- Technology
- Ladies

education:
  courses:
  - course: College of Life
    institution: Void
    year: 2021

# Social/academia Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/erlich-bachman/
# - icon: google-scholar
#   icon_pack: ai
#   link: https://
# - icon: github
#   icon_pack: fab
#   link: https://
# - icon: gitlab
#   icon_pack: fab
#   link: https://
# Link to a PDF of your resume/CV from the About widget.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

I am an arrogant entrepreneur who founded an innovation incubator in my home after the purchase of my airfare collator Aviato. I still holds on to my glory days in the valley, wearing Aviato T-shirts and driving a Ford Escape adorned with Aviato logos.


<!-- ![reviews](img/certifacates.jpg) -->

